/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.List;
import javax.swing.JOptionPane;
import model.User;
import model.UserTableModel;
import model.dao.UserDAO;

/**
 *
 * @author Thiago
 */
public class UserForm extends javax.swing.JFrame {

    /**
     * Creates new form TelaCadastroUsuario
     */
    private boolean fromLogin = false;
    private User user;
    private char flag;
    private UserTableModel tableModel;
    private List<User> list;

    public UserForm(boolean fromLogin) {
        this.fromLogin = fromLogin;
        initComponents();        
        clearFields();
        disableForm();
        search();
        prepareFromLogin();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tpContainer = new javax.swing.JTabbedPane();
        pnList = new javax.swing.JPanel();
        pnTitleL = new javax.swing.JPanel();
        lbNameSearch = new javax.swing.JLabel();
        txNameSearch = new javax.swing.JTextField();
        lbEmailSearch = new javax.swing.JLabel();
        txEmailSearch = new javax.swing.JTextField();
        btSearch = new javax.swing.JButton();
        pnDetails = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbUsers = new javax.swing.JTable();
        btCreate = new javax.swing.JButton();
        btEdit = new javax.swing.JButton();
        btDelete = new javax.swing.JButton();
        pnEdit = new javax.swing.JPanel();
        pnTitleE = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnData = new javax.swing.JPanel();
        lbName = new javax.swing.JLabel();
        lbEmail = new javax.swing.JLabel();
        lbPasswordConfirm = new javax.swing.JLabel();
        lbPassword = new javax.swing.JLabel();
        txName = new javax.swing.JTextField();
        txEmail = new javax.swing.JTextField();
        txPassword = new javax.swing.JPasswordField();
        txPasswordConfirm = new javax.swing.JPasswordField();
        btSave = new javax.swing.JButton();
        btCancel = new javax.swing.JButton();
        cbSuperUser = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Controle de Usuários");
        setMinimumSize(null);

        pnList.setPreferredSize(new java.awt.Dimension(600, 470));
        pnList.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pnListFocusGained(evt);
            }
        });

        pnTitleL.setBackground(new java.awt.Color(73, 88, 103));

        lbNameSearch.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbNameSearch.setForeground(new java.awt.Color(247, 247, 255));
        lbNameSearch.setText("Nome");

        txNameSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txNameSearch.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lbEmailSearch.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbEmailSearch.setForeground(new java.awt.Color(247, 247, 255));
        lbEmailSearch.setText("Email");

        txEmailSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txEmailSearch.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btSearch.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btSearch.setText("Buscar");
        btSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnTitleLLayout = new javax.swing.GroupLayout(pnTitleL);
        pnTitleL.setLayout(pnTitleLLayout);
        pnTitleLLayout.setHorizontalGroup(
            pnTitleLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnTitleLLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnTitleLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txNameSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                    .addComponent(lbNameSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(pnTitleLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnTitleLLayout.createSequentialGroup()
                        .addComponent(lbEmailSearch)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnTitleLLayout.createSequentialGroup()
                        .addComponent(txEmailSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14))))
        );
        pnTitleLLayout.setVerticalGroup(
            pnTitleLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnTitleLLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnTitleLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbEmailSearch)
                    .addComponent(lbNameSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnTitleLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnTitleLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txNameSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txEmailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pnDetails.setBackground(new java.awt.Color(247, 247, 255));

        tbUsers.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbUsers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbUsers);

        btCreate.setBackground(new java.awt.Color(87, 115, 153));
        btCreate.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btCreate.setForeground(new java.awt.Color(247, 247, 255));
        btCreate.setText("Incluir");
        btCreate.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCreateActionPerformed(evt);
            }
        });

        btEdit.setBackground(new java.awt.Color(189, 213, 234));
        btEdit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btEdit.setForeground(new java.awt.Color(73, 88, 103));
        btEdit.setText("Alterar");
        btEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditActionPerformed(evt);
            }
        });

        btDelete.setBackground(new java.awt.Color(254, 95, 85));
        btDelete.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btDelete.setForeground(new java.awt.Color(247, 247, 255));
        btDelete.setText("Excluir");
        btDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnDetailsLayout = new javax.swing.GroupLayout(pnDetails);
        pnDetails.setLayout(pnDetailsLayout);
        pnDetailsLayout.setHorizontalGroup(
            pnDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnDetailsLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnDetailsLayout.setVerticalGroup(
            pnDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnListLayout = new javax.swing.GroupLayout(pnList);
        pnList.setLayout(pnListLayout);
        pnListLayout.setHorizontalGroup(
            pnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnTitleL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnListLayout.setVerticalGroup(
            pnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnListLayout.createSequentialGroup()
                .addComponent(pnTitleL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        tpContainer.addTab("Lista", pnList);

        pnEdit.setPreferredSize(new java.awt.Dimension(600, 470));

        pnTitleE.setBackground(new java.awt.Color(73, 88, 103));
        pnTitleE.setForeground(new java.awt.Color(247, 247, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(247, 247, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Cadastro de Usuário");

        javax.swing.GroupLayout pnTitleELayout = new javax.swing.GroupLayout(pnTitleE);
        pnTitleE.setLayout(pnTitleELayout);
        pnTitleELayout.setHorizontalGroup(
            pnTitleELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnTitleELayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnTitleELayout.setVerticalGroup(
            pnTitleELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnTitleELayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pnData.setBackground(new java.awt.Color(247, 247, 255));

        lbName.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbName.setForeground(new java.awt.Color(87, 115, 153));
        lbName.setText("Nome");

        lbEmail.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbEmail.setForeground(new java.awt.Color(87, 115, 153));
        lbEmail.setText("Email");

        lbPasswordConfirm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbPasswordConfirm.setForeground(new java.awt.Color(87, 115, 153));
        lbPasswordConfirm.setText("Confirmar Senha");

        lbPassword.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbPassword.setForeground(new java.awt.Color(87, 115, 153));
        lbPassword.setText("Senha");

        txName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        txEmail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txEmail.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txEmail.setMargin(new java.awt.Insets(2, 2, 2, 6));

        txPassword.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txPassword.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txPassword.setMargin(new java.awt.Insets(2, 2, 2, 6));

        txPasswordConfirm.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txPasswordConfirm.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txPasswordConfirm.setMargin(new java.awt.Insets(2, 2, 2, 6));

        btSave.setBackground(new java.awt.Color(87, 115, 153));
        btSave.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btSave.setForeground(new java.awt.Color(247, 247, 255));
        btSave.setText("Salvar");
        btSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveActionPerformed(evt);
            }
        });

        btCancel.setBackground(new java.awt.Color(189, 213, 234));
        btCancel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btCancel.setForeground(new java.awt.Color(73, 88, 103));
        btCancel.setText("Cancelar");
        btCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelActionPerformed(evt);
            }
        });

        cbSuperUser.setBackground(new java.awt.Color(247, 247, 255));
        cbSuperUser.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cbSuperUser.setForeground(new java.awt.Color(87, 115, 153));
        cbSuperUser.setText("Super Usuário");

        javax.swing.GroupLayout pnDataLayout = new javax.swing.GroupLayout(pnData);
        pnData.setLayout(pnDataLayout);
        pnDataLayout.setHorizontalGroup(
            pnDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDataLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnDataLayout.createSequentialGroup()
                        .addComponent(cbSuperUser)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lbPasswordConfirm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnDataLayout.createSequentialGroup()
                        .addGroup(pnDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txPasswordConfirm)
                            .addComponent(lbName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txName)
                            .addComponent(txEmail)
                            .addComponent(lbPassword, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txPassword)
                            .addGroup(pnDataLayout.createSequentialGroup()
                                .addComponent(btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                                .addComponent(btCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10))))
        );
        pnDataLayout.setVerticalGroup(
            pnDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDataLayout.createSequentialGroup()
                .addComponent(lbName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txName, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbEmail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbPassword)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbPasswordConfirm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txPasswordConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbSuperUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addGroup(pnDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout pnEditLayout = new javax.swing.GroupLayout(pnEdit);
        pnEdit.setLayout(pnEditLayout);
        pnEditLayout.setHorizontalGroup(
            pnEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnTitleE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnEditLayout.setVerticalGroup(
            pnEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnEditLayout.createSequentialGroup()
                .addComponent(pnTitleE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        tpContainer.addTab("Inclusão / Edição", pnEdit);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tpContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tpContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelActionPerformed
        clearFields();
        disableForm();
        if (this.fromLogin) {
            this.dispose();
        } else {
            tpContainer.setSelectedIndex(0);
        }
        this.user = null;
    }//GEN-LAST:event_btCancelActionPerformed

    private void btCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCreateActionPerformed
       
        create(); 
    }//GEN-LAST:event_btCreateActionPerformed

    private void btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveActionPerformed
        if (validateFields()) {
            this.user.setName(txName.getText());
            this.user.setEmail(txEmail.getText());
            this.user.setPassword(txPassword.getText());
            this.user.setIsSuper(cbSuperUser.isSelected() ? 1 : 0);
            this.save(this.user);
            clearFields();
            searchUser(user);
            disableForm();
            if (this.fromLogin) {
                this.dispose();
            } else {
                tpContainer.setSelectedIndex(0);
            }
            this.user = null;
        }

    }//GEN-LAST:event_btSaveActionPerformed

    private void pnListFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pnListFocusGained
        // TODO add your handling code here:        
    }//GEN-LAST:event_pnListFocusGained

    private void btEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditActionPerformed
        // TODO add your handling code here:
        int line = -1;
        line = tbUsers.getSelectedRow();
        if (line >= 0) {
            int userId = (int) tbUsers.getValueAt(line, 0);
            UserDAO ud = new UserDAO();
            User u = ud.findById(userId);
            update(u);             
        } else {
            JOptionPane.showMessageDialog(null, "É necessário selecionar uma linha para editar");
        }
    }//GEN-LAST:event_btEditActionPerformed

    private void btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDeleteActionPerformed
        int option = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir?", "Atenção", JOptionPane.YES_NO_OPTION);
        if (option==0){
            int line = -1;
            line = tbUsers.getSelectedRow();
            if (line >= 0) {
                int idContato = (int) tbUsers.getValueAt(line, 0);
                delete(idContato);
                tableModel.removeUser(line);
            } else {
                JOptionPane.showMessageDialog(null, "É necessário selecionar uma linha");
            }
        }                
    }//GEN-LAST:event_btDeleteActionPerformed

    private void btSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSearchActionPerformed
        // TODO add your handling code here:
        findUser(txNameSearch.getText(), txEmailSearch.getText());
    }//GEN-LAST:event_btSearchActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
         java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UserForm(false).setVisible(true);
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCancel;
    private javax.swing.JButton btCreate;
    private javax.swing.JButton btDelete;
    private javax.swing.JButton btEdit;
    private javax.swing.JButton btSave;
    private javax.swing.JButton btSearch;
    private javax.swing.JCheckBox cbSuperUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbEmail;
    private javax.swing.JLabel lbEmailSearch;
    private javax.swing.JLabel lbName;
    private javax.swing.JLabel lbNameSearch;
    private javax.swing.JLabel lbPassword;
    private javax.swing.JLabel lbPasswordConfirm;
    private javax.swing.JPanel pnData;
    private javax.swing.JPanel pnDetails;
    private javax.swing.JPanel pnEdit;
    private javax.swing.JPanel pnList;
    private javax.swing.JPanel pnTitleE;
    private javax.swing.JPanel pnTitleL;
    private javax.swing.JTable tbUsers;
    private javax.swing.JTabbedPane tpContainer;
    private javax.swing.JTextField txEmail;
    private javax.swing.JTextField txEmailSearch;
    private javax.swing.JTextField txName;
    private javax.swing.JTextField txNameSearch;
    private javax.swing.JPasswordField txPassword;
    private javax.swing.JPasswordField txPasswordConfirm;
    // End of variables declaration//GEN-END:variables

    private void prepareFromLogin() {
        if (this.fromLogin) {
            tpContainer.remove(0);
            cbSuperUser.setVisible(false);
            create();
        }
    }

    private void create() {
        this.user = new User();
        this.flag = 'I';
        tpContainer.setTitleAt(tpContainer.getTabCount() - 1, "Incluindo Usuário");
        tpContainer.setSelectedIndex(tpContainer.getTabCount() - 1);
        enableForm();
        prepareForm();
    }

    private void update(User user) {
        this.user = user;
        this.flag = 'U';
        tpContainer.setTitleAt(tpContainer.getTabCount() - 1, "Alterando Usuário");
        tpContainer.setSelectedIndex(tpContainer.getTabCount() - 1);
        enableForm();
        prepareForm();
    }

    private void save(User user) {
        UserDAO ud = new UserDAO();
        if (this.flag == 'I') {
            ud.create(user);
            tableModel.addUser(searchUser(user));
        } else {
            ud.update(user);
            tableModel.updateUser(tbUsers.getSelectedRow(), user);
        }
        disableForm();
    }

    private void delete(int id) {
        UserDAO ud = new UserDAO();
        ud.delete(id);        
    }
    
    private void enableForm(){
        txName.setEnabled(true);
        txEmail.setEnabled(true);
        txPassword.setEnabled(true);
        txPasswordConfirm.setEnabled(true);        
        cbSuperUser.setEnabled(true);
        btSave.setEnabled(true);
        btCancel.setEnabled(true);
        tpContainer.setEnabledAt(0, false);
        tpContainer.setEnabledAt(tpContainer.getTabCount()-1, true);
    }
    
    private void disableForm(){
        txName.setEnabled(false);
        txEmail.setEnabled(false);
        txPassword.setEnabled(false);
        txPasswordConfirm.setEnabled(false);        
        cbSuperUser.setEnabled(false);
        btSave.setEnabled(false);
        btCancel.setEnabled(false);
        tpContainer.setTitleAt(tpContainer.getTabCount()-1, "Inclusão / Edição");  
        tpContainer.setEnabledAt(0, true);
        tpContainer.setEnabledAt(tpContainer.getTabCount()-1, false);
    }

    private void clearFields() {
        txName.setText("");
        txEmail.setText("");
        txPassword.setText("");
        txPasswordConfirm.setText("");
        cbSuperUser.setSelected(false);
        txName.requestFocus();
    }

    private void prepareForm() {
        if (this.flag == 'I') {
            clearFields();
        } else {
            txName.setText(user.getName());
            txEmail.setText(user.getEmail());
            txPassword.setText(user.getPassword());
            txPasswordConfirm.setText(user.getPassword());
            cbSuperUser.setSelected(user.getIsSuper() == 1);
            txName.requestFocus();
        }
    }

    private boolean validateFields() {
        if (txName.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nome precisa ser preenchido");
            txName.requestFocus();
            return false;
        }

        if (txEmail.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Email precisa ser preenchido");
            txEmail.requestFocus();
            return false;
        }

        if (txPassword.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Senha precisa ser preenchida");
            txPassword.requestFocus();
            return false;
        }

        if (txPasswordConfirm.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Confirmação de senha precisa ser preenchida");
            txPasswordConfirm.requestFocus();
            return false;
        }

        if (!txPasswordConfirm.getText().equals(txPassword.getText())) {
            JOptionPane.showMessageDialog(null, "Senhas não conferem");
            txPasswordConfirm.requestFocus();
            return false;
        }
        return true;
    }   

    private void search() {
        UserDAO ud = new UserDAO();
        list = ud.list();
        tableModel = new UserTableModel(list);
        tbUsers.setModel(tableModel);
    }

    private User searchUser(User user) {
        UserDAO ud = new UserDAO();
        return ud.findByEmail(user.getEmail());
    }
    
    private void findUser(String name, String email){
        UserDAO ud = new UserDAO();
        list = ud.findByNameOrEmail(name, email);
        tableModel = new UserTableModel(list);
        tbUsers.setModel(tableModel);
    }

}
