/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.sql.Timestamp;

/**
 *
 * @author donathan
 */
public class Schedule {

    int id, userId, notifyUser;
    Date initialDate, finalDate;
    Timestamp createdAt, updatedAt;
    String description;

    public Schedule() {
        this(null, 0, null, null, null);
    }

    public Schedule(User u, int notifyUser, Date initialDate, Date finalDate, String description) {
        if (!(u == null)) {
            this.userId = u.getId();

        }
        this.notifyUser = notifyUser;
        this.initialDate = initialDate;
        this.finalDate = finalDate;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getNotifyUser() {
        return notifyUser;
    }

    public void setNotifyUser(int notifyUser) {
        this.notifyUser = notifyUser;
    }

    public Date getInitialDate() {
        return initialDate;
    }
    
    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
