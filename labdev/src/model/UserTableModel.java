/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Thiago
 */
public class UserTableModel extends AbstractTableModel {

    private static final int COL_ID = 0;
    private static final int COL_NAME = 1;
    private static final int COL_EMAIL = 2;
    private static final int COL_ISSUPER = 3;
    private static final int COL_CREATED_AT = 4;
    private static final int COL_UPDATED_AT = 5;
    List<User> lines;
    private final String[] columns = new String[]{"Id", "Nome", "Email", "Super Usuário", "Criado em", "Modificado em"};

    public UserTableModel(List<User> users) {
        this.lines = new ArrayList<>(users);
    }

    @Override
    public int getRowCount() {
        return lines.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        User u = lines.get(row);
        switch (column) {
            case COL_ID:
                return u.getId();
            case COL_NAME:
                return u.getName();
            case COL_EMAIL:
                return u.getEmail();
            case COL_ISSUPER:
                return (u.getIsSuper()==1)?"Sim":"Não";
            case COL_CREATED_AT:
                return u.getCreatedAt();
            case COL_UPDATED_AT:
                return u.getUpdatedAt();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case COL_ID:
            case COL_ISSUPER:
                return Integer.class;
            case COL_CREATED_AT:
            case COL_UPDATED_AT:
                return Timestamp.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        User u = lines.get(row);
        switch (column) {
            case COL_ID:
                u.setId((Integer) aValue);
                break;
            case COL_NAME:                
                u.setName(aValue.toString());
                break;
            case COL_EMAIL:
                u.setEmail(aValue.toString());
                break;
            case COL_ISSUPER:
                u.setIsSuper((Integer) aValue);
                break;
            case COL_CREATED_AT:
                u.setCreatedAt((Timestamp.valueOf(aValue.toString())));
                break;
            case COL_UPDATED_AT:
                u.setUpdatedAt((Timestamp.valueOf(aValue.toString())));
                break;
            default:
                break;
        }
    }
    
    public User getUser(int lineIndex){
        return lines.get(lineIndex);
    }
    
    public void addUser(User user){
        lines.add(user);
        int lastIndex = getRowCount() - 1;
        fireTableRowsInserted(lastIndex, lastIndex);
    }
    
    public void updateUser(int lineIndex, User user){
        lines.set(lineIndex, user);
        fireTableRowsUpdated(lineIndex, lineIndex);
    }
    
    public void removeUser(int lineIndex){
        lines.remove(lineIndex);
        fireTableRowsDeleted(lineIndex, lineIndex);
    }

}
