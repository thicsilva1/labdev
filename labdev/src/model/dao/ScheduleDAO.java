/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Schedule;
import model.User;

/**
 *
 * @author donathan
 */
public class ScheduleDAO {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");

    public void create(Schedule sche) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = ConnectionFactory.getConnection();

            stmt = con.prepareStatement("INSERT INTO academicschedule.schedule (user_id, initial_date,final_date,notify_user,description) VALUES (?,?,?,?,?);");
            stmt.setInt(1, sche.getUserId());

            stmt.setString(2, sdf.format(sche.getInitialDate()));
            stmt.setString(3, sdf.format(sche.getFinalDate()));
            stmt.setInt(4, sche.getNotifyUser());
            stmt.setString(5, sche.getDescription());

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Criado com sucesso!");
        } catch (Exception ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }

    public void update(Schedule sche) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = ConnectionFactory.getConnection();

            stmt = con.prepareStatement("UPDATE schedule SET initial_date = ? ,final_date = ?,notify_user = ? ,description = ? WHERE id = ?");

            stmt.setString(1, sdf.format(sche.getInitialDate()));
            stmt.setString(2, sdf.format(sche.getFinalDate()));
            stmt.setInt(3, sche.getNotifyUser());
            stmt.setString(4, sche.getDescription());
            stmt.setInt(5, sche.getId());

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar: " + ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }

    public void delete(int id) {

        Connection con = null;

        PreparedStatement stmt = null;

        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("DELETE FROM schedule WHERE id = ?");
            stmt.setInt(1, id);

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Excluido com sucesso!");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir: " + ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }

    public List<Schedule> read(User user) {

        Connection con = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Schedule> sches = new ArrayList<>();
        try {
            con = ConnectionFactory.getConnection();
            String query = "SELECT * FROM schedule";

            if (user.getIsSuper() != 1) {
                query += " where user_id=?";
            }

            stmt = con.prepareStatement(query);

            if (user.getIsSuper() != 1) {
                stmt.setInt(1, user.getId());
            }

            rs = stmt.executeQuery();

            while (rs.next()) {

                Schedule sche = new Schedule();

                sche.setId(rs.getInt("id"));
                sche.setUserId(rs.getInt("user_id"));
                sche.setInitialDate(rs.getTimestamp("initial_date"));
                sche.setFinalDate(rs.getTimestamp("final_date"));
                sche.setDescription(rs.getString("description"));
                sche.setNotifyUser(rs.getInt("notify_user"));
                sche.setCreatedAt(rs.getTimestamp("created_at"));
                sche.setUpdatedAt(rs.getTimestamp("updated_at"));
                sches.add(sche);
            }

        } catch (Exception ex) {
            Logger.getLogger(ScheduleDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }

        return sches;

    }

    public List<Schedule> readForData(String dataInicial, String dataFinal, User user) {
        Connection con = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        String query = "SELECT * FROM schedule";

        List<Schedule> sches = new ArrayList<>();
        try {
            con = ConnectionFactory.getConnection();
            if (dataInicial != null && dataFinal != null) {
                query += " where initial_date >= ? and final_date <= ?";
            } else if (dataInicial != null) {
                query += " where initial_date >= ?";
            } else if (dataFinal != null) {
                query += " where final_date <= ?";
            }

            if (user.getIsSuper() != 1) {
                query += " and user_id=?";
            }

            stmt = con.prepareStatement(query);

            if (dataInicial != null && dataFinal != null) {
                stmt.setString(1, dataInicial);
                stmt.setString(2, dataFinal);
                if (user.getIsSuper() != 1) {
                    stmt.setInt(3, user.getId());
                }
            } else if (dataInicial != null) {
                stmt.setString(1, dataInicial);
                if (user.getIsSuper() != 1) {
                    stmt.setInt(2, user.getId());
                }
            } else if (dataFinal != null) {
                stmt.setString(1, dataFinal);
                if (user.getIsSuper() != 1) {
                    stmt.setInt(2, user.getId());
                }
            }

            rs = stmt.executeQuery();

            while (rs.next()) {

                Schedule sche = new Schedule();

                sche.setId(rs.getInt("id"));
                sche.setUserId(rs.getInt("user_id"));
                sche.setInitialDate(rs.getTimestamp("initial_date"));
                sche.setFinalDate(rs.getTimestamp("final_date"));
                sche.setDescription(rs.getString("description"));
                sche.setNotifyUser(rs.getInt("notify_user"));
                sche.setCreatedAt(rs.getTimestamp("created_at"));
                sche.setUpdatedAt(rs.getTimestamp("updated_at"));
                sches.add(sche);
            }

        } catch (Exception ex) {
            Logger.getLogger(ScheduleDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }

        return sches;

    }

    public List<Schedule> readForDesc(String desc) {
        Connection con = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        String query = "SELECT * FROM schedule where description like ? ;";

        List<Schedule> sches = new ArrayList<>();
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(query);
            stmt.setString(1, "%" + desc + "%");

            rs = stmt.executeQuery();

            while (rs.next()) {

                Schedule sche = new Schedule();

                sche.setId(rs.getInt("id"));
                sche.setUserId(rs.getInt("user_id"));
                sche.setInitialDate(rs.getTimestamp("initial_date"));
                sche.setFinalDate(rs.getTimestamp("final_date"));
                sche.setDescription(rs.getString("description"));
                sche.setNotifyUser(rs.getInt("notify_user"));
                sche.setCreatedAt(rs.getTimestamp("created_at"));
                sche.setUpdatedAt(rs.getTimestamp("updated_at"));
                sches.add(sche);
            }

        } catch (Exception ex) {
            Logger.getLogger(ScheduleDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }

        return sches;

    }

    public Schedule findByInterval(String initialDate, String finalDate) {
        Schedule sche = new Schedule();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String query = "SELECT * FROM schedule where initial_date <= ? and final_date >= ?";
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(query);            
            stmt.setString(1, finalDate);
            stmt.setString(2, initialDate);
            rs = stmt.executeQuery(); 
            if (rs.next()==false){
                sche = null;
            } else {
                do {
                    sche.setId(rs.getInt("id"));
                    sche.setUserId(rs.getInt("user_id"));
                    sche.setInitialDate(rs.getTimestamp("initial_date"));
                    sche.setFinalDate(rs.getTimestamp("final_date"));
                    sche.setDescription(rs.getString("description"));
                    sche.setNotifyUser(rs.getInt("notify_user"));
                    sche.setCreatedAt(rs.getTimestamp("created_at"));
                    sche.setUpdatedAt(rs.getTimestamp("updated_at"));
                    break;
                } while (rs.next());
            }
   

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return sche;

    }

    public Schedule findById(int id) {
        Schedule sch = new Schedule();

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement("SELECT * FROM `schedule` WHERE id=?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                sch.setId(rs.getInt("id"));
                sch.setInitialDate(rs.getTimestamp("initial_date"));
                sch.setFinalDate(rs.getTimestamp("final_date"));
                sch.setDescription(rs.getString("description"));
                sch.setNotifyUser(rs.getInt("notify_user"));
                sch.setCreatedAt(rs.getTimestamp("created_at"));
                sch.setUpdatedAt(rs.getTimestamp("updated_at"));
                return sch;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuário: " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return sch;
    }

    public List<Schedule> findByPeriodOrDescription(String initialDate, String finalDate, String description, User user) {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String query = "SELECT * FROM schedule where (initial_date>=? and final_date<=? and description like ?)";

        List<Schedule> sches = new ArrayList<>();
        try {
            if (user.getIsSuper()!=1){
                query+=" and user_id=?";
            }
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(query);
            stmt.setString(1, initialDate);
            stmt.setString(2, finalDate);
            stmt.setString(3, "%" + description + "%");
            if (user.getIsSuper()!=1){
                stmt.setInt(4, user.getId());
            }

            rs = stmt.executeQuery();

            while (rs.next()) {

                Schedule sche = new Schedule();

                sche.setId(rs.getInt("id"));
                sche.setUserId(rs.getInt("user_id"));
                sche.setInitialDate(rs.getTimestamp("initial_date"));
                sche.setFinalDate(rs.getTimestamp("final_date"));
                sche.setDescription(rs.getString("description"));
                sche.setNotifyUser(rs.getInt("notify_user"));
                sche.setCreatedAt(rs.getTimestamp("created_at"));
                sche.setUpdatedAt(rs.getTimestamp("updated_at"));
                sches.add(sche);
            }

        } catch (Exception ex) {
            Logger.getLogger(ScheduleDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }

        return sches;
    }

}
