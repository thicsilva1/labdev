/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.User;

/**
 *
 * @author donathan
 */
public class UserDAO {

    private final String INSERT = "INSERT INTO `user` (email, password, name, is_super) VALUES (?, ?, ?, ?);";
    private final String UPDATE = "UPDATE `user` SET email=?, password=?, name=?, is_super=? WHERE id=?;";
    private final String DELETE = "DELETE FROM `user` WHERE id=?;";
    private final String LIST = "SELECT * FROM `user`;";
    private final String FINDBYID = "SELECT * FROM `user` WHERE id=?";
    private final String FINDBYEMAIL = "SELECT * FROM `user` WHERE email=?";
    private final String FINDBYNAME = "SELECT * FROM `user` WHERE name=?";    

    public void create(User u) {
        if (u != null) {
            Connection con = null;
            PreparedStatement stmt = null;
            try {
                con = ConnectionFactory.getConnection();
                stmt = con.prepareStatement(INSERT);
                stmt.setString(1, u.getEmail());
                stmt.setString(2, u.getPassword());
                stmt.setString(3, u.getName());
                stmt.setInt(4, u.getIsSuper());
                stmt.execute();

                JOptionPane.showMessageDialog(null, "Criado com sucesso!");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao inserir usuário " + e.getMessage());
            } finally {
                ConnectionFactory.closeConnection(con, stmt);
            }
        } else {
            System.out.println("Usuário enviado por parâmetro está vazio");
        }

    }

    public void update(User u){
        
        if (u != null) {
            Connection con = null;
            PreparedStatement stmt = null;
            try {
                con = ConnectionFactory.getConnection();
                stmt = con.prepareStatement(UPDATE);
                stmt.setString(1, u.getEmail());
                stmt.setString(2, u.getPassword());
                stmt.setString(3, u.getName());
                stmt.setInt(4, u.getIsSuper());
                stmt.setInt(5, u.getId());
                stmt.execute();
                JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao inserir usuário " + e.getMessage());
            } finally {
                ConnectionFactory.closeConnection(con, stmt);
            }
        } else {
            JOptionPane.showMessageDialog(null,"Usuário enviado por parâmetro está vazio");
        }

    }

    public void delete(int id){

        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(DELETE);
            stmt.setInt(1, id);

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Excluido com sucesso!");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }

    public User findByEmail(String email) {
        User user = new User();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(FINDBYEMAIL);
            stmt.setString(1, email);
            rs = stmt.executeQuery();
            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setIsSuper(rs.getInt("is_super"));
                user.setCreatedAt(rs.getTimestamp("created_at"));
                user.setUpdatedAt(rs.getTimestamp("updated_at"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuário: " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return user;

    }

    public User findById(int id) {
        User user = new User();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(FINDBYID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setIsSuper(rs.getInt("is_super"));
                user.setCreatedAt(rs.getTimestamp("created_at"));
                user.setUpdatedAt(rs.getTimestamp("updated_at"));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuário: " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return user;

    }

    public List<User> list() {
        Connection con = null ;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<>();
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(LIST);
            rs = stmt.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setIsSuper(rs.getInt("is_super"));
                user.setCreatedAt(rs.getTimestamp("created_at"));
                user.setUpdatedAt(rs.getTimestamp("updated_at"));
                users.add(user);
            }
        } catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return users;

    }
    
    public List<User> findByNameOrEmail(String name, String email) {
        Connection con = null ;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<>();
        String query = "SELECT * FROM `user`";
        
        try {
            if (!name.trim().isEmpty() && !email.trim().isEmpty()){
                query +=" WHERE name like ? and email like ?;"; 
            } else if (!name.trim().isEmpty()){
                query +=" WHERE name like ?;";
            } else if (!email.trim().isEmpty()){
                query +=" WHERE email like ?;";
            }
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(query);
             if (!name.trim().isEmpty() && !email.trim().isEmpty()){
                stmt.setString(1, '%' + name + '%');
                stmt.setString(2, '%' + email + '%');
            } else if (!name.trim().isEmpty()){
                stmt.setString(1, '%' + name + '%');
            } else if (!email.trim().isEmpty()){
                stmt.setString(1, '%' + email + '%');
            }
            rs = stmt.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setIsSuper(rs.getInt("is_super"));
                user.setCreatedAt(rs.getTimestamp("created_at"));
                user.setUpdatedAt(rs.getTimestamp("updated_at"));
                users.add(user);
            }
        } catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + e.getMessage());
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return users;

    }

}
