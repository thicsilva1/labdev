/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Samuelson
 */
public class ConnectionFactory {

    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3307/academicschedule?verifyServerCertificate=False&useSSL=False";
    private static final String USER = "root";
    private static final String PASS = "";

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException e) {            
            throw new ClassNotFoundException("Driver não foi encontrado " + e.getMessage());
        } catch (SQLException e){
            JOptionPane.showMessageDialog(null, "Erro ao conectar com a base de dados " + e.getMessage());
            throw new SQLException("Erro ao conectar com a base de dados " + e.getMessage());
        }
    }

    public static void closeConnection(Connection con) {
        try {
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            System.out.println("Não foi possível fechar a conexão com o banco de dados " + e.getMessage());
        }
    }

    public static void closeConnection(Connection con, PreparedStatement stmt) {
        

        try {
            if (con != null) {
                closeConnection(con);
            }
            if (stmt != null) {
                stmt.close();
            }

        } catch (Exception e) {
            System.out.println("Não foi possível fechar o Statement "+ e.getMessage());
        }
    }

    public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs) {

        

        try {
            if (con!=null|stmt!=null){
                closeConnection(con, stmt);
            }

            if (rs != null) {
                rs.close();
            }

        } catch (Exception e) {
            System.out.println("Não foi possível fechar o ResultSet " + e.getMessage());
        }
    }

}
