/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Thiago
 */
public class ScheduleTableModel extends AbstractTableModel{
    
    private static final int COL_ID = 0;
    private static final int COL_DATE_INI = 1;
    private static final int COL_DATE_FIN = 2;
    private static final int COL_DESCRIPTION = 3;
    private static final int COL_NOTIFY = 4;
    List<Schedule> lines;
    private final String[] columns = new String[]{"Id", "Data Inicial", "Data Final", "Descrição", "Notificar"};

    public ScheduleTableModel(List<Schedule> lines) {
        this.lines = new ArrayList<> (lines);
    }
    

    @Override
    public int getRowCount() {
        return lines.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Schedule s = lines.get(row);        
        switch(column){
            case COL_ID:
                return s.getId();
            case COL_DATE_INI:
                return s.getInitialDate();
            case COL_DATE_FIN:
                return s.getFinalDate();
            case COL_DESCRIPTION:
                return s.getDescription();
            case COL_NOTIFY:
                return s.getNotifyUser()==1?"Sim":"Não";
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex){
            case COL_ID:
            case COL_NOTIFY:
                return Integer.class;
            case COL_DATE_INI:
            case COL_DATE_FIN:
                return Date.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        Schedule s = lines.get(row);
        switch(column){
            case COL_ID:
                s.setId((Integer)aValue);
                break;
            case COL_DATE_INI:                
                s.setInitialDate((Date)aValue);
                break;
            case COL_DATE_FIN:
                s.setFinalDate((Date)aValue);
                break;
            case COL_DESCRIPTION:
                s.setDescription(aValue.toString());
                break;
            case COL_NOTIFY:
                s.setNotifyUser((Integer)aValue);
            default:
                break;
        }
    }
    
    public Schedule getSchedule(int lineIndex){
        return lines.get(lineIndex);
    }
    
    public void addSchedule(Schedule s){
        lines.add(s);
        int lastIndex = getRowCount()-1;
        fireTableRowsInserted(lastIndex, lastIndex);
    }
    
    public void updateSchedule(int lineIndex, Schedule s){
        lines.set(lineIndex, s);
        fireTableRowsUpdated(lineIndex, lineIndex);
    }
    
    public void removeSchedule(int lineIndex){
        lines.remove(lineIndex);
        fireTableRowsDeleted(lineIndex, lineIndex);
    }    

    
}
