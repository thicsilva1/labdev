/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 *
 * @author donathan
 */
public class Botao extends JButton {

    private Prateleira prateleira;
    private List<Botao> botoes;

    public Botao(Action action) {
        super(action);
    }

    public Botao() {
        
    }

    public void setBotoes(List<Botao> botoes) {
        this.botoes = botoes;
    }

    public List<Botao> getBotoes() {
        return botoes;
    }
    
    
    
    

    public Botao(Icon icon) {
        super(icon);
    }

    public Botao(String string) {
        super(string);
    }

    public Botao(Prateleira prateleira, String string, Icon icon) {
        super(string, icon);
        this.prateleira = prateleira;
    }

    public Prateleira getPrateleira() {
        return prateleira;
    }

    public void setPrateleira(Prateleira prateleira) {
        this.prateleira = prateleira;
    }

}
