/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author donathan
 */
public class Prateleira {
    private int qntMaxEspaco,idPrateleira;
    private final List<Caixa> caixas = new ArrayList<>();
    private Estante estante;

    public Prateleira(int QntMaxEspaco) {
        this.qntMaxEspaco = QntMaxEspaco;
    }

    public Estante getEstante() {
        return estante;
    }

    public void setEstante(Estante estante) {
        this.estante = estante;
    }
    
    
    public void addCaixa(Caixa caixa){
        if(caixas.size() < qntMaxEspaco){
            caixas.add(caixa);
            caixa.setPrateleira(this);
        }else{
            System.out.println("estante cheia");
        }
    }
    

    public int getQntMaxEspaco() {
        return qntMaxEspaco;
    }

    public void setQntMaxEspaco(int qntMaxEspaco) {
        this.qntMaxEspaco = qntMaxEspaco;
    }


    public int getIdPrateleira() {
        return idPrateleira;
    }

    public void setIdPrateleira(int id) {
        this.idPrateleira = id;
    }

    public List<Caixa> getCaixas() {
        return caixas;
    }



    
    
    
    
    
    
    
    
    
    
}
