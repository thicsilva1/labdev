/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author donathan
 */
public class Caixa {
    private int id,qntCamundongo,idTipoCaixa;
    private Prateleira prateleira;

    public Prateleira getPrateleira() {
        return prateleira;
    }

    public void setPrateleira(Prateleira prateleira) {
        this.prateleira = prateleira;
    }

    public Caixa(int qntCamundongo,TipoCaixa tipoCaixa) {
        this.qntCamundongo = qntCamundongo;
        this.idTipoCaixa = tipoCaixa.getIdTipoCaixa();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQntCamundongo() {
        return qntCamundongo;
    }

    public void setQntCamundongo(int qntCamundongo) {
        this.qntCamundongo = qntCamundongo;
    }

    public int getIdTipoCaixa() {
        return idTipoCaixa;
    }

    protected void setIdTipoCaixa(int idTipoCaixa) {
        this.idTipoCaixa = idTipoCaixa;
    }
    
    
    
    
    
    
}
