/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author donathan
 */
public class Estante {

    private int idEstante;
    private final List<Prateleira> prateleiras = new ArrayList<>();

    public Estante(int qntPrateleira, int tamanhoPrateleira) {
        criarPrateleira(qntPrateleira, tamanhoPrateleira);
    }

    private void criarPrateleira(int qntPrateleira, int tamanhoPrateleira) {
        for (int i = 0; i < qntPrateleira; i++) {
            addPrateleira(new Prateleira(tamanhoPrateleira));
        }
    }

    private void addPrateleira(Prateleira prateleira) {
        prateleiras.add(prateleira);
        prateleira.setEstante(this);

    }

    public int getIdEstante() {
        return idEstante;
    }

    public void setIdEstante(int idEstante) {
        this.idEstante = idEstante;
    }

    public Prateleira getPrateleira(int i) {
        return prateleiras.get(i);
    }
    public List<Prateleira> getListPrateleira(){
        return prateleiras;
    }
    
    public int getQuantidadePrateleiras(){
        return prateleiras.size();
    }


}
